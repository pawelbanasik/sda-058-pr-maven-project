package com.sda.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class FirstJson {

	public static void main(String[] args) throws FileNotFoundException {

		// JSONObject obj = new JSONObject();
		//
		// obj.put("Field", "Value1");
		// obj.put("Number", 23);
		//
		// JSONObject obiekt_do_tablicy = new JSONObject();
		// obiekt_do_tablicy.put("key", "1");
		//
		// JSONArray arr = new JSONArray();
		// arr.add(obiekt_do_tablicy);
		// arr.add(23);
		// arr.add(24);
		// arr.add(25);
		// arr.add(26);
		// arr.add(27);
		// arr.add(28);
		// obj.put("Array", arr);
		//
		// System.out.println(obj.toJSONString());

		// Zadanie 1
		// JSONObject obj = new JSONObject();
		// obj.put("pierwszy", "przyklad");
		// obj.put("drugi", "inny przypadek");
		// obj.put("liczba", 5);
		//
		// JSONArray arr = new JSONArray();
		// arr.add(1);
		// arr.add(2);
		// arr.add(3);
		// arr.add(4);
		// arr.add(5);
		// obj.put("tablica", arr);
		//
		// JSONObject obj2 = new JSONObject();
		// obj2.put("x", 5);
		// obj2.put("y", 17);
		//
		//
		//
		// System.out.println(obj.toJSONString());

		// Zadanie 2
		// Random random = new Random();
		// int[] myArray = new int[random.nextInt(100)];
		// JSONArray jsonArray = new JSONArray();
		//
		// for (int i = 0; i < myArray.length; i++) {
		// myArray[i] = random.nextInt(100);
		// // System.out.println(myArray[i]);
		// jsonArray.add(myArray[i]);
		//
		// }
		//
		// JSONObject obiekt = new JSONObject();
		// obiekt.put("tablica", jsonArray);
		//
		// System.out.println(obiekt.toJSONString());
		//
		// try {
		// PrintWriter pw = new PrintWriter("resources/randomInts.json");
		// pw.println(obiekt.toJSONString());
		// pw.close();
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// }

		// Zad 3 po mojemu (jeszcze musze to sprawdzic)
		String[] array = new String[52];
		PrintWriter pw = new PrintWriter("resources/words.json");
		Scanner sc = new Scanner(new File("resources\\words.txt\\"));
		int i = 0;

		
		
		while (sc.hasNextLine()) {
			array[i] = sc.nextLine();
			array[i + 1] = sc.nextLine();
			if (i % 2 == 0) {
				JSONObject object = new JSONObject();
				object.put(array[i], array[i + 1]);
				pw.println(object);
			}
			i += 2;
		}
		
		
		
		pw.close();
	}

}
